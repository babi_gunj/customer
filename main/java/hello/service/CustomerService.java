package hello.service;

import hello.dto.CustomerDto;
import hello.mapper.CustomerMapper;
import hello.model.Customer;
import hello.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.http.HttpStatus;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerMapper mapper;

    @Transactional
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    //TODO: Implement methods for each controller method. Note that each of them has to call different method from Service.
    @Transactional
    public long saveCustomer(CustomerDto customerDto) {
        Customer customer = mapper.toEntity(customerDto);
        customerRepository.save(customer);
        return customer.getId();
    }

    public CustomerDto getCustomer(long id) {
        System.out.println(customerRepository.findById(id));
        return mapper.toDto(customerRepository.findById(id).orElse(null));
    }

    public CustomerDto getCustomer(String lastname){
        return mapper.toDto((customerRepository.findByLastName(lastname).iterator().next()));
    }

    public CustomerDto updateCustomer(long id, CustomerDto dto) {
        Customer advertisement = customerRepository.findById(id).orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND));
        mapper.update(advertisement, dto);
        return mapper.toDto(customerRepository.save(advertisement));
    }

    @Transactional
    @Modifying
    public void deleteCustomer(long id) {
        customerRepository.deleteById(id);
    }
    @Transactional
    @Modifying
    public void deleteCustomerByLastName(Customer customer){
        customerRepository.delete(customer);
    }
}
