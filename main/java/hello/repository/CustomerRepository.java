package hello.repository;

import java.util.List;
import java.util.Optional;

import com.sun.xml.bind.v2.model.core.ID;
import hello.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
    @Override
    List<Customer> findAll();

    List<Customer> findByLastName(String lastName);


    Optional<Customer> findById(ID id);

    //TODO: Add code that will be necessary to implement all methods in service

   // List<Customer> findByFirstAndLastName(Customer customer);

}
