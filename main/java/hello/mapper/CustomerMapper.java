package hello.mapper;

import hello.dto.CustomerDto;
import hello.model.Customer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomerMapper {

    //TODO: Implement method that maps entity to dto
    public CustomerDto toDto(Customer customer) {
        CustomerDto customerDto = new CustomerDto();
        if (customer != null) {
            BeanUtils.copyProperties(customer, customerDto, CustomerDto.class);
        }
        return customerDto;
    }


    //TODO: Implement method that maps dto to entity

    public Customer toEntity(CustomerDto dto) {
        Customer customer = new Customer();
        if (dto != null) {
            BeanUtils.copyProperties(dto, customer, Customer.class);
        }
        return customer;
    }




    //TODO: Implement method that maps List of entities to List of dto

    public List<CustomerDto> toDtoList(List<Customer> customers) {
        return customers.stream().map(this::toDto).collect(Collectors.toList());
    }

    //TODO: Implement method that maps List of dto to List of entities

    public List<Customer> toCustomerList(List<CustomerDto> customerDto){
        return customerDto.stream().map(this::toEntity).collect(Collectors.toList());
    }

    //TODO: Implement method that updates entity with dto data

    public void update(Customer customer, CustomerDto dto) {
        if (dto != null) {
            customer.setFirstName(dto.getFirstName());
            customer.setLastName(dto.getLastName());
        }
    }



}
