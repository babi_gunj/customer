package hello.controller;


import hello.dto.CustomerDto;
import hello.model.Customer;
import hello.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class CustomerController {

    @Autowired
    CustomerService customerService;

    //@RequestMapping("/")
    /*public List<Customer> hello() {
        return Collections.emptyList();
    }*/
    @RequestMapping("/hello")
    public String sayHello(){
        return "hello";
    }

    //TODO: All logic has to be implemented in service!

    //TODO: Create GET method that retrieves all customers

    @GetMapping({"/customers","/"})
    public String findAllCustomers(Model model) {
        model.addAttribute("customers", customerService.getAllCustomers());
        return "customers";
    }

    //TODO: Create GET method that gets customer by its ID
    @GetMapping("/customers/{id}")
    public String getCustomer(@PathVariable long id, Model model) {
        CustomerDto customerDto = customerService.getCustomer(id);
        model.addAttribute("customer", customerDto);
        return "single-customer";
    }

    //TODO: Create GET method that gets customer by search key (name, surname, etc.)
   /* @GetMapping("/customers/{last}")
    public String getCustomer(@PathVariable String lastname,Model model){
        CustomerDto customerDto = customerService.getCustomer(lastname);
        model.addAttribute("customer", customerDto);
        return "single-customer";
    }
*/
    //TODO: Create POST method to saves new customer
    @PostMapping("/newCustomer")
    public RedirectView addCustomer(@ModelAttribute CustomerDto customerDto){
        long id = customerService.saveCustomer(customerDto);
        return new RedirectView("/customers/" + id);
    }
    @GetMapping("/customers/create")
    public String createNewAdvertisement(CustomerDto customerDto) {
        return "save-customer";
    }


    //TODO: Create PUT method to update existing customer. Note! If user tries to update not existing customer, throw an exception
    @PostMapping("/customers/update/{id}")
    public RedirectView updateCustomer(@PathVariable long id, @ModelAttribute CustomerDto customerDto) {
        customerService.updateCustomer(id, customerDto);
        return new RedirectView("/customers/" + id);
    }
    @GetMapping("/customers/update/{id}")
    public String update(@PathVariable long id, Model model) {
        model.addAttribute("customerDto", customerService.getCustomer(id));
        return "update-customer";
    }
    //TODO: Create DELETE method that deletes customer by id
    @DeleteMapping("/customers/{id}")
    public RedirectView deleteCustomer(@PathVariable long id) {
        customerService.deleteCustomer(id);
        return new RedirectView("/");
    }

    //TODO: Create DELETE method that deletes customer by any other key
    @DeleteMapping("/ads/{customer}")
    public RedirectView deleteCustomerByName(@PathVariable Customer customer) {
        customerService.deleteCustomerByLastName(customer);
        return new RedirectView("/");
    }

}
